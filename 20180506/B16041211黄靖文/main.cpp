//
//  main.cpp
//  旋转矩阵0506.cpp
//
//  Created by 黄靖文 on 2018/5/14.
//  Copyright © 2018年 黄靖文. All rights reserved.
//

#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;



void Print(int arr[][4], int row, int col)
{
    int i,j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < col; j++)
        {
            cout<<arr[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
}

void clockwise(int a[][4], int b[][4],int row,int col)
{
    int i,j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < col; j++)
        {
            b[col-1-j][i] = a[i][j];
        }
        
    }
    Print(b,row,col);
}
int main()
{
    int c[4][4], d[4][4], i, j;
    srand((unsigned)time(NULL));
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            c[i][j] = rand()%100+1;
            
        }
    }
    Print(c, 4, 4);
    
    clockwise(c, d, 4, 4);
    return 0;
}

