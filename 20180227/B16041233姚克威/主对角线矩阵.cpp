#include<iostream>
#include<iomanip>
using namespace std;
void matrix2()
{
	int i,j;
	int a[4][4];
	a[0][0]=1;
	for(i=0;i<4;i++)
	{
		if(i>0)
			a[i][i]=a[i-1][3]+1;
		for(j=i+1;j<4;j++)
		{
			a[i][j]=a[i][j-1]+1;
		}
	}
	for(j=0;j<4;j++)
	{
		for(i=j+1;i<4;i++)
		{
			a[i][j]=a[j][i];
		}
	}
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			cout<<setw(4)<<a[i][j];
		}
		cout<<endl;
	}
}
int main()
{
	matrix2();
	return 0;
}
