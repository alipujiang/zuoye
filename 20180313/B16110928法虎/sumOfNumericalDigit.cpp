#include <iostream>

using namespace std;


int sumOfNumber(int n)
{
    int sum=0;
    for( int i=1; i<=n; i++)
    {
       int j=i;
       sum+=j%10;
       while(j/10)
       {
           j=j/10;
           sum+=j%10;
       }
    }
    return sum;
}


int main()
{
    int n,sum;
    cin >> n;
    sum=sumOfNumber(n);
    cout << sum << endl;
    return 0;
}
