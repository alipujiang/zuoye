#include<stdio.h>
#include<assert.h>
#include<string.h>
char *strcat(char *str1,char *str2)
{
	char *p1;
	char *p2;
	assert(str1 != NULL && str2 != NULL);
	p1=str1;
	p2=str2;
	while(*p1!='\0')
		p1++;
	while(*p2!='\0')
	{
		*p1=*p2;
		p1++;
		p2++;
	}
	*p1='\0';
	return str1;
}
int main()
{
	char a[1000];
	char b[1000];
	memset(a, '\0', sizeof(a));
    memset(b, '\0', sizeof(b));
    scanf("%s",a);
	scanf("%s",b);
	printf("%s\n",strcat(a,b));
	return 0;
}
