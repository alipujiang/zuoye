#include <iostream>
#include <iomanip>
using namespace std;
void Print(int** b,int n); 
void Clockwise(int** a,int **b,int n);
void Anticlockwise(int** a,int **b,int n);
int main()
{
	int n=0,m=0;
	int i,j; 
	cout<<"请输入矩阵阶数:"<<endl;
	cin>>n;
	int** a=new int*[n];
	int** b=new int*[n];
	for(int i=0;i<n;i++)
	{
		a[i]=new int[n];
        b[i]=new int[n];
	}
	cout<<"请输入矩阵中各个数的值:"<<endl;
	for(i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			 cin>>m;
			 a[i][j]=m;
		}
	}
	cout<<"原矩阵为:"<<endl;
	Print(a,n);
	Clockwise(a,b,n);
	Anticlockwise(a,b,n);
	for(int k=0;k<n;k++)
	{
		delete[] b[k];
		delete[] a[k];
	}
	delete[] b;
	delete[] a;
    return 0;
}
void Print(int** b,int n)
{
	for(int p=0;p<n;p++)
	{
	  for(int q=0;q<n;q++)
		{
			cout<<b[p][q]<<setw(5)<<" ";
		}
	  cout<<endl;
	}
}
void Clockwise(int** a,int **b,int n)
{
	cout<<"顺时针旋转90度后矩阵是:"<<endl;
   for(int i=0;i<n;i++)
	{
	  for(int j=0;j<n;j++)
		{
			b[j][n-1-i]=a[i][j];
		}
	}
   Print(b,n);
}
void Anticlockwise(int** a,int **b,int n)
{
	cout<<"逆时针旋转90度后矩阵是:"<<endl;
   for(int i=0;i<n;i++)
	{
	  for(int j=0;j<n;j++)
		{
			b[n-1-j][i]=a[i][j];
		}
	}
   Print(b,n);
}
