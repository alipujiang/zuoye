#include<stdio.h>
#include<stdlib.h>
#include<string.h>

char *encryption(const char *s) {
	int str_len = strlen(s);
	char *temp = malloc(sizeof(char) * (str_len + 1));
	for (int i = 0; i < str_len; i++) {
		if (s[i] <= 'z' && s[i] >= 'a')
			temp[i] = (s[i] + 2 - 'a') % ('z' - 'a' + 1) + 'a';
		else if (s[i] <= 'Z' && s[i] >= 'A')
			temp[i] = (s[i] + 2 - 'A') % ('Z' - 'A' + 1) + 'A';
		else if (s[i] >= '0' && s[i] <= '9')
			temp[i] = (s[i] + 2 - '0') % ('9' - '0' + 1) + '0';
	}
	temp[str_len] = '\0';
	return temp;
}

int main() {
	char s1[100];
	gets(s1);
	char *result = encryption(s1);
	puts(result);
	return 0;
}