//利用c语言中的fgetc(),fputc()实现文本文件复制
//从main(int argc,char **argv)中获取srcfile和destfile
//文件操作三要素 fopen fputc fgetc feof fclose
//argc:一个命令行有多少个参数
#include<stdio.h>
#include<stdlib.h>
int main(int argc,char** argv)
{
//FILE *srcFile=0,*destFile=0;
//int ch=0;
//int i=0;
if(argc!=3)
{
 printf("参数个数有误！");
}
if(argc==3)
{
 char *srcfile=argv[1];
 char *destfile=argv[2];
 FILE *fp1=fopen(srcfile,"r");
    FILE *fp2=fopen(destfile,"w");
 if(fp1==0)
 {
  printf("file not exist!");
  return -1;
 }
// FILE *fp2=fopen(destfile,"w");
 while(1)
 {
  char temp=fgetc(fp1);
  if(feof(fp1))
   break;
  fputc(temp,fp2);
 }
 fclose(fp1);
 fclose(fp2);
}
}
